// import the library
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSearch, faPencilAlt, faTrashAlt, faListAlt, faUser, faBuilding, faUniversity,
  faLandmark, faSquare, faUsers
} from '@fortawesome/free-solid-svg-icons';


library.add(
  faUser, faUsers, faBuilding, faUniversity,
  faLandmark, faListAlt, faSquare,
  faSearch, faPencilAlt, faTrashAlt,
  // more icons go here
);
