
import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import PrivateRoute from './private-route'

import MainLayout from '../components/layouts/Main'
import LoginLayout from '../components/layouts/Login'

import Login from '../pages/Login'
import Dashboard from '../pages/Dashboard'
import Entity from '../pages/Entity'
import Kind from '../pages/Kind'
import Area from '../pages/Area'
import Persona from '../pages/Persona'

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Login} layout={LoginLayout} />
      <Route exact path="/login" component={Login} layout={LoginLayout} />
      <PrivateRoute exact path="/dashboard" component={Dashboard} layout={MainLayout} />
      <PrivateRoute exact path="/entity" component={Entity} layout={MainLayout} />
      <PrivateRoute exact path="/kind" component={Kind} layout={MainLayout} />
      <PrivateRoute exact path="/area" component={Area} layout={MainLayout} />
      <PrivateRoute exact path="/persona" component={Persona} layout={MainLayout} />
      <Route path="*" component={() => <h1>Página não localizada</h1>} />
    </Switch>
  </BrowserRouter>
)

export default Routes
