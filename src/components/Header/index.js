import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default props =>
  <header className="shadow-lg bg-white d-none d-sm-flex flex-column my-1 py-0 px-3">
    <h2 className="mt-2">
      <FontAwesomeIcon icon={props.icon} className="p-1" />
      {props.title}
    </h2>
    <p className="lead text-muted mt-0">{props.subtitle}</p>
  </header>
