import React from 'react';
import NavTop from '../../Nav'

// import { Container } from './styles';

export default ({ children }) => (
  <>
    <div>
      <NavTop />
      {children}
    </div>
  </>
)

