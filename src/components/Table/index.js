import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

// import { Container } from './styles';

export default function TableBase ({ data }) {
  // const headersName = Object.keys(d  ata[0])

  const head = data.map((key, value) => Object.keys(key))
  // let head = data.map((key, value) => ({ value })
  //=> Object.keys(key))
  console.log('Valor Props: ', head[0])
  // const headersName = hns[0]
  // data.map((key, value) => Object.keys(key))

  const headersName = head[0]
  const trem = Object.keys(data)
  console.log('valor de data: ', trem)
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            {headersName.map(h => (
              <TableCell key={h}>{h}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody></TableBody>
      </Table>
    </TableContainer>
  )
}
