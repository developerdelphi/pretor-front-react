import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { userLogged } from '../../service/auth'

const user = userLogged()



class NavTop extends Component {
  container = React.createRef();
  state = {
    open: false
  };

  handleDropDownClick = () => {
    this.setState(state => {
      return {
        open: !state.open
      };
    });
  }

  handleClickOutSide = event => {
    if (
      this.container.current &&
      !this.container.current.contains(event.target)
    ) {
      this.setState({
        open: false
      });
    }
  }

  componentDidMount () {
    document.addEventListener("mousedown", this.handleClickOutside);
  }
  componentWillUnmount () {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  render () {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary p-0" ref={this.container}>
        <Link className="navbar-brand text-light" to="/dashboard">Pretor 2.0</Link>
        <div className="d-flex flex-row justify-content-end w-100" >
          <ul className="navbar-nav d-flex justify-content-around">
            <li className="nav-item d-flex ">
              <NavLink strict activeClassName="active" className="nav-link" to="/entity">Entidades</NavLink>
            </li>
            <li className="nav-item d-flex ">
              <NavLink strict activeClassName="active" className="nav-link" to="/area">Áreas</NavLink>
            </li>
            <li className="nav-item d-flex ">
              <NavLink strict activeClassName="active" className="nav-link" to="/kind">Classes</NavLink>
            </li>
            <li className="nav-item d-flex ">
              <NavLink strict activeClassName="active" className="nav-link" to="/persona">Pessoas</NavLink>
            </li>
            <li className="nav-item dropdown">
              <Link className="nav-link dropdown-toggle"
                to="#" id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                onClick={this.handleDropDownClick}
              >
                {user.name}
              </Link>
              <div
                className={this.state.open ? "dropdown-menu show" : "dropdown-menu"}
                aria-labelledby="navbarDropdown"
              >
                <NavLink exact activeClassName="active" className="dropdown-item" to="/" >Perfil</NavLink>
                <NavLink exact activeClassName="active" className="dropdown-item" to="/" >Outra ação</NavLink>
                <div className="dropdown-divider"></div>
                <Link className="dropdown-item" to="/" >Sair</Link>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default NavTop

