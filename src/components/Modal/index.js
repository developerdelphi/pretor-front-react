import React from 'react';

// import { Container } from './styles';

const Modal = props => {
  let { show } = props

  if (!show) return null

  const handleShowModal = () => {
    show = true
  }

  const handleOnClose = e => {
    props.onClose && props.onClose(e)
  };

  return (
    <div className="modal show d-block">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Nova mensagem</h5>
            <button onClick={e => { handleOnClose(e) }} type="button" className="close" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <form>
              <div className="form-group">
                <label className="col-form-label">Destinatário:</label>
                <input type="text" className="form-control" id="recipient-name" />
              </div>
              <div className="form-group">
                <label className="col-form-label">Mensagem:</label>
                <textarea className="form-control" id="message-text"></textarea>
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              onClick={e => { handleOnClose(e) }}
              // onClick={handleShowModal}
              className="btn btn-secondary"
              data-dismiss="modal"
            >Fechar</button>
            <button type="button" className="btn btn-primary">Enviar</button>
          </div>
        </div>

      </div>
    </div>
  );
}

export default Modal
