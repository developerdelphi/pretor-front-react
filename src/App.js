import React from 'react'
import { dom } from '@fortawesome/fontawesome-svg-core'

import Routes from './routes/index'

dom.watch()

function App () {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
