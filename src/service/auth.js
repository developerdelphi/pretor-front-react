export const TOKEN_KEY = 'PretorToken'
export const TOKEN_USER = 'PretorUser'
export const isAuthenticated = () => {
  const token = localStorage.getItem(TOKEN_KEY)
  return token ? true : false
}
export const getToken = () => {
  let token = localStorage.getItem(TOKEN_KEY) || ''
  // console.log(token)
  return (token = token.replace('"', ''))
}
export const login = token => {
  localStorage.setItem(TOKEN_KEY, token)
}
export const logout = () => {
  localStorage.removeItem(TOKEN_KEY)
}

export const userLogged = () => {
  return JSON.parse(localStorage.getItem(TOKEN_USER))
}
