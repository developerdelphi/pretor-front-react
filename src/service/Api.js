import axios from 'axios'
import { getToken } from './auth'

const Api = axios.create({
  baseURL: process.env.REACT_APP_API_URL
})

Api.interceptors.request.use(async config => {
  const token = getToken()
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
    // console.log('api', token)
  }
  return config
})

export default Api
