import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom'
import { FaEnvelope, FaKey, FaBalanceScale } from 'react-icons/fa'
// import { toast } from 'react-toastify'
// import 'react-toastify/dist/ReactToastify.css';
import { showToast } from '../../components/toast'
import logo from '../../assets/logo-01.svg'
import imgBackground from '../../assets/imgBackground.jpg'

// import './styles.css';
import api from '../../service/Api'

// toast.configure()

export default function Login () {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [emailError, setEmailError] = useState([])
  const [passwordError, setPasswordError] = useState([])
  const history = useHistory()

  // Apagar o storage quando logout e reseta para login
  localStorage.removeItem('PretorToken')
  localStorage.removeItem('PretorUser')

  async function signIn (e) {
    e.preventDefault()

    const dataLogin = { email, password }
    setEmailError('')
    setPasswordError('')

    await api.post('auth/login', dataLogin)
      .then(response => {
        if (response.data) {
          showToast({ type: 'success', message: 'Autorização confirmada.' })
          showToast({ type: 'info', message: 'Login realizado' })
          localStorage.setItem('PretorToken', JSON.stringify(response.data.data.token))
          localStorage.setItem('PretorUser', JSON.stringify(response.data.data.user))
          history.push('/dashboard')
          // console.log(JSON.stringify(response.data))
        }
      })
      .catch(error => {
        const responseData = error.response.data
        const responseStatus = error.response.status
        if (responseStatus === 422) {
          const errors = responseData.errors
          showToast({ type: 'error', message: 'Confirme as crendenciais de acesso.' })
          errors.email ? setEmailError(errors.email) : setEmailError('')
          errors.password ? setPasswordError(errors.password) : setPasswordError('')
          // console.log('erros msg; ', errors.password, JSON.stringify(responseData.errors.email))
        }
        if (responseStatus === 401) {
          showToast({ type: 'error', message: responseData.message })
        }
      })
  }
  return (
    <div className="d-flex flex-row justify-content-center m-0 p-0">
      <img src={imgBackground} alt="" className="img-fluid position-absolute h-100 w-100" />
      <div className="d-flex align-items-center justify-content-center height100vh">
        <div className="card shadow-sm w-75">
          <div className="card-header bg-primary grid">
            <div className="row">
              <div className="d-flex col-4">
                <FaBalanceScale size={70} className="text-secondary align-items-center justify-content-center" />
              </div>
              <div className="d-flex col-8 align-items-center justify-content-end">
                <h3 className="text-secondary"> PRETOR 2.0 </h3>
              </div>
              <div className="col-12">
                <h4 className="text-center">
                  Autenticação de Usuário
                </h4>
              </div>
            </div>
          </div>
          <div className="card-body">
            <form onSubmit={signIn}>
              <div className="input-group my-2">
                <div className="input-group-prepend">
                  <span className="input-group-text" id="my-addon">
                    <FaEnvelope className="text-primary" size={18} />
                  </span>
                </div>
                <input
                  className="form-control"
                  type="text"
                  placeholder="Informe e-mail"
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                />

              </div>
              <span className={emailError ? "is-invalid badge badge-danger" : ""}>{emailError}</span>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text" id="my-addon">
                    <FaKey className="text-primary" size={18} />
                  </span>
                </div>
                <input
                  className="form-control"
                  type="password"
                  placeholder="Informe a senha"
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                />
              </div>
              <div className={passwordError ? "is-invalid badge badge-danger" : ""}>{passwordError}</div>
              <button className="btn btn-primary w-100 mt-3 font-bold" type="submit">Entrar</button>
              <hr />
              <Link to="/register" className="badge badge-light">
                Recuperar acesso
                  </Link>
            </form>
          </div>
        </div>
      </div >
    </div >
  );
}
