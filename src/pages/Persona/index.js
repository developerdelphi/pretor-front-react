
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import api from '../../service/Api'

import Header from '../../components/Header';


// import { Container } from './styles';

export default function Persona () {
  const [records, setRecords] = useState([])
  const history = useHistory()

  useEffect(() => {
    api
      .get('personas')
      .then(response => {
        setRecords(response.data.data)
      })
      .catch(error => {
        const responseData = error.response.data
        const responseStatus = error.response.status
        if (responseStatus === 401) {
          // setRecords([])
          history.push('/')
        }
        console.log(responseData)
      })
  }, [history])

  return (
    <>
      <Header title="Pessoas" icon="square" subtitle="Cadastro de pessoas físicas e jurídicas" />
      <div className="container-fluid">
        <table className="table table-sm table-hover table-striped">
          <thead className="bg-primary">
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th className="text-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            {records.map(r => (
              <tr key={r.id}>
                <td>{r.id}</td>
                <td>{r.name}</td>
                <td className="text-right">
                  <div className="btn-group btn-group-sm" role="group">
                    <button type="button" onClick={() => { }} className="btn btn-mini btn-light"><FontAwesomeIcon className="text-primary" icon="search" /></button>
                    <button type="button" className="btn btn-mini btn-light"><FontAwesomeIcon icon="pencil-alt" className="text-primary" /></button>
                    <button type="button" className="btn btn-mini btn-light"><FontAwesomeIcon icon="trash-alt" className="text-primary" /></button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  )
}


