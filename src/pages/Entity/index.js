import React, { useState, useEffect, useRef } from 'react'
import { FaSearch, FaTrash, FaPencilAlt, FaBuilding } from 'react-icons/fa'
import { useHistory } from 'react-router-dom'

import Modal from '../../components/Modal'
import Header from '../../components/Header'
import api from '../../service/Api'

export default function Entity () {
  const [records, setRecords] = useState([])
  const [showModal, setShowModal] = useState(false)

  // const modalRef = useRef(null);
  const history = useHistory()

  const handleShowModal = () => {
    setShowModal(true)
  }

  const handleCloseShowModal = e => {
    setShowModal(!showModal)
    console.log('Fechar modarl: ', showModal)
  }

  useEffect(() => {
    api
      .get('entities')
      .then(response => {
        setRecords(response.data.data)
        // response.data ? setRecords(response.data) : setRecords([])
      })
      .catch(error => {
        const responseData = error.response.data
        const responseStatus = error.response.status
        if (responseStatus === 401) {
          // setRecords([])
          history.push('/')
        }
        console.log(responseData)
      })
  }, [history])

  return (
    <>
      <Modal show={showModal} onClose={(e) => handleCloseShowModal(e)} />
      <Header
        title="Entidades"
        subtitle="Cadastro de entidades responsáveis pelo processo"
        icon="landmark"
      />
      <div className="container-fluid">
        <table className="table table-sm table-hover table-striped">
          <thead className="bg-primary">
            <tr>
              <th>#</th>
              <th>Entidade</th>
              <th className="text-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            {records.map(r => (
              <tr key={r.id}>
                <td>{r.id}</td>
                <td>{r.name}</td>
                <td className="text-right">
                  <div className="btn-group btn-group-sm" role="group">
                    <button type="button" onClick={handleShowModal} className="btn btn-mini btn-light"><FaSearch className="text-primary" /></button>
                    <button type="button" className="btn btn-mini btn-light"><FaPencilAlt className="text-primary" /></button>
                    <button type="button" className="btn btn-mini btn-light"><FaTrash className="text-primary" /></button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  )
}
