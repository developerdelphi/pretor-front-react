import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './styles/index.scss'
import './styles/icons/fontawesome'


ReactDOM.render(
  // <React.StrictMode>
  <App />,
  // </React.StrictMode>,
  document.getElementById('root')
);
